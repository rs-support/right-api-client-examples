#!/usr/bin/env ruby
# Setup
require 'right_api_client'
client = RightApi::Client.new(:email => 'my@email.com', :password => 'my_password', :account_id => 'my_account_id')
recipe_name = "app::do_update_code"

# --------------------------------------------------------------------
# Find Server 
# In terms of locating the server you can either use and ID or the Name/Deployment.
# You should only follow one route
# ------------------------------------------------------------------------
# By Deployment
deployment = client.deployments.index(:filter => ['name==My DeploymentName '])[0]
server = servers.index(:filter => ['name==PHP App Server'])[0].current_instance

# By ID
server = deployment.servers.index(:id => '123123123').show.current_instance
# ---------------------------------------------------------------------

# Run Recipe:
task = utility_server.show.run_executable("recipe_name=#{recipe_name}")

# Check Status
status = task.show.summary
puts "Recipe Status: '#{status}'"

# Loop Through and wait until Finished
while true
  # Grab Current Status
  status = task.show.summary
  case status
    # Potential Statuses that indicate it is still running
    when "Retrieving cookbooks", "Converging", "Querying tags", "Scheduling execution of #{recipe_name}", "Enqueueing #{recipe_name} for execution", "Preparing #{recipe_name} for execution"
      sleep(5)
    when "completed: #{recipe_name}"
      puts "Recipe completed!" 
      break
    else
      puts "Recipe Failed!"
  end
end

