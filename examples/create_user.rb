#!/usr/bin/env ruby
# Setup
require 'right_api_client'
client = RightApi::Client.new(:email => 'my@email.com', :password => 'my_password', :account_id => 'my_account_id')

# Setup Parameters
params = {
  :company => 'RightScale',
  :email => 'example@rightscale.com',
  :first_name => 'right',
  :last_name => 'scale',
  :phone => '1231231234',
  :password => "Sup3rSecur3"
}
# Create User
user = client.users.create(:user => params )

# Add Roles to User
client.permissions.create(:permission => {:role_title => 'observer', :user_href => user.href})
client.permissions.create(:permission => {:role_title => 'actor', :user_href => user.href})
client.permissions.create(:permission => {:role_title => 'server_login', :user_href => user.href})
client.permissions.create(:permission => {:role_title => 'server_superuser', :user_href => user.href})

