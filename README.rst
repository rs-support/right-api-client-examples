.. image:: http://support.rightscale.com/@api/deki/site/logo.png
   :target: http://support.rightscale.com



========
Overview
========

This is a collection of examples of the RightScale API Ruby Gem Client (right_api_client). With a valid user and account you should be able to run all of these examples with little to no modification. Pull requests welcome!



Useful Links
========

* `RightScale API Reference <http://reference.rightscale.com/api1.5/index.html>`_

* `RightScale API Guide <http://support.rightscale.com/12-Guides/RightScale_API_1.5>`_

* `Right Api Client <https://github.com/rightscale/right_api_client>`_

